#!/usr/bin/python
import sys
c = 0
err = """==========================================
USEAGE
$ python bincut.py [filename]
or
$ python bincut.py [filename] -o [output file]
==========================================
"""
try:
	f = open(sys.argv[1],"r")
	a = f.read().split("\n")
	b = ""
	for i in a:
		b += i[9:-18]+" "
	c = 1
	try:
		if sys.argv[2] == "-o":
			c = 0
			z = open(sys.argv[3],"wb")
			z.write(b+"\n")
			z.close()
		else:
			print err
	except IndexError:
		if c:
			print b
		else:
			print err
except IndexError:
	print err
